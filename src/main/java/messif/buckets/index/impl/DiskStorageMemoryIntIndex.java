/*
 *  This file is part of PPP-Codes library: http://disa.fi.muni.cz/results/software/ppp-codes/
 *
 *  PPP-Codes library is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  PPP-Codes library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with PPP-Codes library.  If not, see <http://www.gnu.org/licenses/>.
 */
package messif.buckets.index.impl;

import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import messif.buckets.BucketStorageException;
import messif.buckets.index.Search;
import messif.buckets.storage.impl.DiskStorage;
import messif.objects.LocalAbstractObject;
import messif.objects.keys.IntegerKey;

/**
 * Implementation of disk (long) index that stores the indexed data in a sorted array and keeps the
 * keys to be compared always in memory.
 * 
 * @author David Novak, Masaryk University, Brno, Czech Republic, novak.david@gmail.com
 */
public class DiskStorageMemoryIntIndex implements Serializable {

    /** Class serial id for serialization. */
    private static final long serialVersionUID = 1021301L;

    /** Maximal length of the dynamic index; if exceeded, then the indexes are moved to static arrays */
    private static final int MAX_DYNAMIC_LENGTH = 1024 * 64;

    //****************** Attributes ******************//

    /** Storage associated with this index */
    private DiskStorage<LocalAbstractObject> storage;
   
    /** Ordered linked index of addresses into the storage */
    private transient List<IntKeyAddressPair> dynamicIndex;
        
    /** Fixed static ordered arrays of keys and corresponding object positions in the storage. */
    private int [] staticIndex;    
    private long [] staticPositions;
    

    //****************** Constructor ******************//

    /**
     * Creates a new instance of LongStorageMemoryIndex for the specified storage.
     * @param storage the storage to associate with this index
     */
    public DiskStorageMemoryIntIndex(DiskStorage<LocalAbstractObject> storage) {
        this.storage = storage;
        this.dynamicIndex = new ArrayList<>(MAX_DYNAMIC_LENGTH);
        this.staticIndex = new int [0];
        this.staticPositions = new long [0];
    }

    
    @Override
    public void finalize() throws Throwable {
        storage.finalize();
        super.finalize();
    }

    public void destroy() throws Throwable {
        storage.destroy();
        storage = null;
    }
    
    // **********************    (De)serialization methods     ************************** //
    private void writeObject(java.io.ObjectOutputStream out) throws IOException {
        clearDynamicIndex();
        out.defaultWriteObject();
    }

    private void readObject(java.io.ObjectInputStream in) throws IOException, ClassNotFoundException {
        in.defaultReadObject();
        this.dynamicIndex = new ArrayList<>(MAX_DYNAMIC_LENGTH);
    }

    /**
     * If the dynamic index is not empty then this methods moves all its data to the static arrays (sorted merge).
     */
    private void clearDynamicIndex() {
        if (dynamicIndex.isEmpty()) {
            return;
        }
        int [] newStaticIndex = new int [staticIndex.length + dynamicIndex.size()];
        long [] newStaticPositions = new long [staticIndex.length + dynamicIndex.size()];
                
        // Merge sort data
        int from1 = 0;
        int to1 = staticIndex.length;
        int fromDest = 0;
        Iterator<IntKeyAddressPair> iterator = dynamicIndex.iterator();
        IntKeyAddressPair nextDynamic = iterator.next();
        while (nextDynamic != null) {
            // If the current item of this collection is bigger than the current item of the added collection
            if (from1 >= to1 || staticIndex[from1] > nextDynamic.key) {
                newStaticIndex[fromDest] = nextDynamic.key;
                newStaticPositions[fromDest ++] = nextDynamic.position;
                nextDynamic = iterator.hasNext() ? iterator.next() : null;
            } else {
                newStaticIndex[fromDest] = staticIndex[from1];
                newStaticPositions[fromDest ++] = staticPositions[from1 ++];
            }
        }
        // Copy the remaining data from the old static array
        if (from1 < to1) {
            System.arraycopy(staticIndex, from1, newStaticIndex, fromDest, to1 - from1);
            System.arraycopy(staticPositions, from1, newStaticPositions, fromDest, to1 - from1);
        }
        
        // set newly created indexes
        staticIndex = newStaticIndex;
        staticPositions = newStaticPositions;
        dynamicIndex.clear();
    }
    
    //******************    Search and order      ******************//

    protected int binarySearch(boolean inStaticIndex, int key, int low, int high, boolean indicateFailure) throws IndexOutOfBoundsException, IllegalStateException {
        while (low <= high) {
            int mid = (low + high) >>> 1;
            int cmp = Integer.compare(key, inStaticIndex ? staticIndex[mid] : dynamicIndex.get(mid).key);

            if (cmp > 0) {
                low = mid + 1;
            } else if (cmp < 0) {
                high = mid - 1;
            } else {
                return mid; // key found
            }
        }

        // Key not found
        if (indicateFailure)
            return -(low + 1);
        else
            return low;
    }

    
    // ******************     Index access methods     ****************** //

    /**
     * Given a set of integer keys, this methods returns corresponding objects from the storage (not necessarily
     *  in given order).
     * @param keys object integer keys
     * @return iterator corresponding objects from the storage (not necessarily in the same order)
     */
    public Iterator<LocalAbstractObject> getObjects(Collection<Integer> keys) {
        long positions [] = new long [keys.size()];
        int index = 0;
        for (int key : keys) {
            // try to find the key in the static index
            int keyIndex = binarySearch(true, key, 0, staticIndex.length - 1, true);
            if (keyIndex >= 0) {
                positions[index ++] = staticPositions[keyIndex];
            } else if (! dynamicIndex.isEmpty() && ((keyIndex = binarySearch(false, key, 0, dynamicIndex.size() - 1, true)) > 0)) {
                positions[index ++] = dynamicIndex.get(keyIndex).position;
            }
        }
        return storage.read((positions.length == index) ? positions : Arrays.copyOf(positions, index));
    }
    
    /**
     * Returns the maximal currently indexed key.
     * @return the maximal currently indexed key
     */
    public int getMaxKey() {
        return Math.max(staticIndex.length > 0 ? staticIndex[staticIndex.length - 1] : -1, 
                dynamicIndex.isEmpty() ? -1 : dynamicIndex.get(dynamicIndex.size() - 1).key);
    }
    
    /**
     * Searches for the point where to insert the object <code>object</code>.
     * @param key key of the object to be inserted
     * @return the point in the array where to put the object
     * @throws BucketStorageException if there was a problem determining the point
     */
    protected int insertionPoint(int key) throws BucketStorageException {
        return binarySearch(false, key, 0, dynamicIndex.size() - 1, false);
    }

    
    protected int extractKey(LocalAbstractObject object) {
        return DiskStorageMemoryIntIndex.getIntegerID(object);
    }
    
    /**
     * Adds an object to the storage and it's integer key to the local key index.
     * @param object object to be added to this index and its storage
     * @return true if the insertion worked ok
     * @throws BucketStorageException if the storage does not accept the object
     */
    public boolean add(LocalAbstractObject object) throws BucketStorageException {
        // Search for the position where the object is added into index
        int key = extractKey(object);
        int pos = insertionPoint(key);

        dynamicIndex.add(pos, new IntKeyAddressPair(key, storage.store(object).getAddress()));
        if (dynamicIndex.size() > MAX_DYNAMIC_LENGTH) {
            clearDynamicIndex();
        }

        return true;
    }

    public Search<LocalAbstractObject> getAllObjects() {
        return storage.search();
    }
    
    /**
     * Class encapsulating the key and long position in the storage.
     */
    protected static class IntKeyAddressPair {

        public final int key;
        
        public final long position;

        public IntKeyAddressPair(int key, long position) {
            this.key = key;
            this.position = position;
        }
    }

    /**
     * Reads and returns the integer ID supposedly stored in the {@link IntegerKey} within the given object. If not
     *  stored, the string locator is tried to be converted to integer. If not, the locator hash code is returned.
     * @param object object to get integer ID for
     * @return integer ID for given object
     */
    public static int getIntegerID(LocalAbstractObject object) {
        try {
            if (object.getObjectKey() instanceof IntegerKey) {
                return ((IntegerKey) object.getObjectKey()).key;
            }
            return Integer.valueOf(object.getLocatorURI());
        } catch (NumberFormatException ex) {
            return object.getLocatorURI().hashCode();
        }
    }
}
