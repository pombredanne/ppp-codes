/*
 *  This file is part of PPP-Codes library: http://disa.fi.muni.cz/results/software/ppp-codes/
 *
 *  PPP-Codes library is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  PPP-Codes library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with PPP-Codes library.  If not, see <http://www.gnu.org/licenses/>.
 */
package pppcodes.ids;

import java.util.Collection;
import java.util.Iterator;
import messif.buckets.BucketStorageException;
import messif.buckets.index.Search;
import messif.objects.LocalAbstractObject;

/**
 * 
 * @author David Novak, Masaryk University, Brno, Czech Republic, novak.david@gmail.com
 */
public interface IDObjectRAStorage<T extends LocalAbstractObject> {
    
    /**
     * Given an integer ID, this method returns the stored object.
     * @param id number identifier of the object to be returned
     * @return stored object identified by the identifier passed
     */
    public T readObject(int id);

    /**
     * Given a list of integer IDs, this method returns the corresponding stored objects. The
     *  returned objects does not have to be in the same order! The objects that were not found
     *  are simply not returned by the iterator.
     * @param ids number identifiers of the objects to be returned
     * @return iterator over the object identified by the identifier passed
     */
    public Iterator<T> readObjects(Collection<Integer> ids);
    
    /**
     * Stores given object to this storage and returns true, if it works.
     * @param object object to be stored
     * @return true if the addition was successful
     * @throws BucketStorageException if the storage fails
     */
    public boolean storeObject(T object)  throws BucketStorageException;
    
    /**
     * Iterator over all the stored objects.
     * @return iterator over all the stored objects.
     */
    public Search<T> getAllObjects();
    
    /**
     * Preprocess the object before inserting it to the storage.
     * @param object object to be preprocessed 
     * @return the same preprocessed object
     */
    public T preprocessObject(T object);
}
