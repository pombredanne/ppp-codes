/*
 *  This file is part of PPP-Codes library: http://disa.fi.muni.cz/results/software/ppp-codes/
 *
 *  PPP-Codes library is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  PPP-Codes library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with PPP-Codes library.  If not, see <http://www.gnu.org/licenses/>.
 */
package pppcodes.index;

import java.io.IOException;
import java.nio.BufferOverflowException;
import java.nio.BufferUnderflowException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.NoSuchElementException;
import java.util.Set;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicLong;
import java.util.logging.Level;
import messif.algorithms.AlgorithmMethodException;
import messif.objects.util.AbstractObjectIterator;
import messif.operations.QueryOperation;
import mindex.MetricIndex;
import mindex.MetricIndexes;
import mindex.navigation.SplitConfiguration;
import mindex.navigation.VoronoiInternalCell;
import mindex.navigation.VoronoiLeafCell;
import mindex.distance.PartialQueryPPPDistanceCalculator;
import mindex.distance.QueryPPPDistanceCalculator;
import mindex.navigation.MinQueryDistanceRevisor;
import pppcodes.PPPCodeIndex;

/**
 * Abstract class encapsulates a leaf node of a cluster tree for a PPP-Code index.
 *
 * @author David Novak, Masaryk University, Brno, Czech Republic, novak.david@gmail.com
 */
public class PPPCodeLeafCell extends VoronoiLeafCell<PPPCodeObject> implements MinQueryDistanceRevisor {

    /** Class id for serialization. */
    private static final long serialVersionUID = 412201L;
    
    /** Number of bits occupied by an instance of this class */
    public static final int LEAF_SIZE_BITS = 16 * Byte.SIZE;

    /** Initially, size of the data storage is set to a INIT_SIZE_COEF-fraction of the maximal size */
    private static final int INIT_SIZE_COEF = 16;
    
    /** Size of the data storage is in turn increased by this coefficient if necessary */
    private static final float ARRAY_INCREMENT_COEF = 1.5f;
    
    
    /** Memory data storage */
    protected volatile byte [] data;
    
    /** Length of the data array in bits */
    protected int positionBits;
    
    /** New write buffer addressed in bits */
    protected volatile transient ByteBufferBits writeBufferBits;
    
    /** 
     * Set of locators (IDs) of deleted objects; it is kept in memory and during serialization (or
     * compaction) these IDs are removed from the actual data. 
     */
    protected volatile transient int [] deletedIndexes;
    
    /**
     * Constructor given all parameters mandatory for the parent class. The storage and the covered interval are set to null.
     * The pivot combination is NOT PASSED because it is stored in the internal cell.
     * @param mIndex M-Index logic
     * @param parentNode parent node of this node
     * pivotCombination the pivot combination corresponding to this cluster, e.g. [3,2,4] for cluster C_{3,2,4}
     */
    public PPPCodeLeafCell(MetricIndex mIndex, VoronoiInternalCell parentNode) {
        super(mIndex, parentNode);
    }

    /**
     * Copy constructor with exception of the local data objects which are passed separately.
     * 
     * @param copyFrom a leaf node to copy all inherited fields from.
     * @param positionBits current length of the data in bits
     * @param data byte array with the PPP-Codes + IDs
     */
    public PPPCodeLeafCell(VoronoiLeafCell copyFrom, int positionBits, byte [] data) {
        super(copyFrom);
        this.positionBits = positionBits;
        this.data = data;
    }
    
    private void writeObject(java.io.ObjectOutputStream out) throws IOException {
        if (! isValid()) {
            throw new IOException("Trying to serialize leaf cell which is not valid");
        }
        consolidateData(null);
        // write position in bits
        out.writeInt(getOccupationInBits());
        if ((writeBufferBits != null) && (writeBufferBits.position() > 0)) {
            out.write(Arrays.copyOfRange(data, 0, writeBufferBits.position()));
        } else{
            if (data != null) {
                out.write(data);
            }
        }
    }

    private void readObject(java.io.ObjectInputStream in) throws IOException, ClassNotFoundException {
        positionBits = in.readInt();
        if (positionBits > 0) {
            data = new byte [PPPCodeReadWriter.bitsToBytes(positionBits)];
            in.readFully(data);
        }
    }
    
    /**
     * Returns the R/W manager for PPPCOde objects.
     */
    private PPPCodeReadWriter getPPPCodeRW() {
        return ((PPPCodeIndex) mIndex).getPPPCodeReadWriter();
    }
    
    @Override
    public PPPCodeIndex getMIndex() {
        return (PPPCodeIndex) mIndex;
    }
    
    // ***************************      Data and storage manipulation (implementation)    ********************* //

    /**
     * Returns current minimum data occupation (if the byte array is shrunk to minimum)
     * @return data occupation in bytes
     */
    public final int getOccupationInBytes() {
        return (writeBufferBits == null) ? ((data == null) ? 0 : data.length) : writeBufferBits.position();
    }    

    /**
     * Returns current minimum data occupation (if the byte array is shrunk to minimum)
     * @return data occupation in bits
     */
    public final int getOccupationInBits() {
        return (writeBufferBits == null) ? positionBits : writeBufferBits.positionInBits();
    }

    public static int bitsToBytes(int bits) {
        // it is important to retype the BYTE_BITS to float
        return (int) Math.ceil(bits / (float) PPPCodeReadWriter.BYTE_BITS);
    }
    
    /**
     * This method calculates how many leaf nodes would be created if this leaf is to be split.
     * @param objectsToInsert list of objects to be inserted in addition to the already stored data
     * @param myLevel level of this leaf node
     * @return number of leaf nodes that would be created if this node is split (and becomes inner node)
     */
    private int calculateBranching(List<PPPCodeObject> objectsToInsert, short myLevel) {
        if (myLevel >= mIndex.getMaxLevel()) {
            return 0;
        }
        Set<Short> branching = new HashSet<>();
        // calculate branching from the data already 
        if (data != null) {
            Iterator<PPPCodeObject> allObjects = getAllObjects();
            while (allObjects.hasNext()) {
                branching.add(allObjects.next().getIndexAt(0));
            }
        }
        
        if (! objectsToInsert.isEmpty()) {
            int objectsPPPCodeIndex = objectsToInsert.get(0).getPPPLength() - mIndex.getMaxLevel() + myLevel;
            Iterator<PPPCodeObject> iterator = objectsToInsert.iterator();
            while (iterator.hasNext()) {
                branching.add(iterator.next().getIndexAt(objectsPPPCodeIndex));
            }
        }
        return branching.size();
    }
    
    
    /**
     * Size of the allocated space in {@link #data} is increased on request - this method 
     *  returns the new capacity, if it can be increased.
     * @return the new capacity of this node's leaf storage, if it can be increased
     */
    protected final int getNewCapacityBytes(List<PPPCodeObject> objectsToInsert, short myLevel, int bytesToCarry) {
        int maxCapacity = getPPPCodeRW().getOptimalLeafCapacityBytes(
                calculateBranching(objectsToInsert, myLevel), myLevel);
        
        int retVal = (data != null) ? data.length : (maxCapacity / INIT_SIZE_COEF);
        if (retVal <= 0) {
            return bytesToCarry;
        }
        while (retVal < bytesToCarry) {
            retVal = (int) (retVal * ARRAY_INCREMENT_COEF);
            if ((retVal > maxCapacity) && (myLevel < mIndex.getMaxLevel())) {
                retVal = maxCapacity;
                break;
            }
        }
        return retVal;
    }
    
    /**
     * Size of the allocated space in {@link #data} is increased on request - this method 
     *  returns the new capacity, if it can be increased.
     * @return the new capacity of this node's leaf storage, if it can be increased
     */
    private boolean ensureWriteOf(List<PPPCodeObject> objects, short myLevel) {
        // WARNING: THIS METHOD ASSUMES THAT THERE IS ONLY ONE LOCATOR IN EACH OBJECT
        // calculate the number of bytes that are to be stored after writing given list of objects
        int bytesToCarry = bitsToBytes(getOccupationInBits() + objects.size() * getPPPCodeRW().getPPPCodeObjectBitSize(myLevel));
        
        // if it fits, do not anything else
        if (bytesToCarry <= (data != null ? data.length : 0)) {
            return true;
        }
        
        return increaseCapacity(objects, myLevel, bytesToCarry);
    }
    
    private boolean increaseCapacity(List<PPPCodeObject> objects, short myLevel, int bytesToCarry) {
        // if the buffer cannot be increased as requested, then do NOTHING and return false
        // (do not insert any data but let the leaf be split and only then data inserted)
        int newSize = getNewCapacityBytes(objects, myLevel, bytesToCarry);
        if (bytesToCarry > newSize) {
            return false;
        }        
        
        // create new data of the new sufficient size
        byte [] newArray = new byte [newSize];
        ByteBufferBits newWriteBufferBits = getPPPCodeRW().createNewBuffer(newArray);
        if (data != null) {
            System.arraycopy(data, 0, newArray, 0, data.length);
            newWriteBufferBits.positionInBits(getOccupationInBits());
        }

        this.data = newArray;
        this.writeBufferBits = newWriteBufferBits;
        return true;
    }

    /**
     * Tries to insert all the objects into the bucket managed by this LeafCell. In PPPCode, the 
     *  insert always either inserts all objects or none.
     * @param objects to be inserted to the bucket
     * @return information about the split to be done (number of inserted objects and other configuration)
     * @throws AlgorithmMethodException 
     */
    @Override
    public synchronized SplitConfiguration insertObjects(List<PPPCodeObject> objects) throws AlgorithmMethodException {
        if (! ensureWriteOf(objects, getLevel())) {
            return new SplitConfiguration(false, 0, objects, null);
        }

        PPPCodeReadWriter serializer = getPPPCodeRW();
        int pppSufixLength = mIndex.getMaxLevel() - getLevel();        
        Iterator<PPPCodeObject> iterator = objects.iterator();
        
        // number of IDs inserted into this cell
        int insertedObjs = 0;
        // number of PPPCodeObjects from the given list inserted into this cell
        int pppObjCount = 0;
        while (iterator.hasNext()) {
            PPPCodeObject next = iterator.next();
            while (true) {
                try {
                    next.writeToBytes(writeBufferBits, serializer, pppSufixLength);
                    break;
                } catch (BufferOverflowException ex) {
                    // try to increase the buffer size
                    if (increaseCapacity(objects.subList(pppObjCount, objects.size()), getLevel(), data.length + 1)) {
                        continue;
                    }
                    objectCount += insertedObjs;
//                    Logger.getLogger(PPPCodeLeafCell.class.getName()).log(Level.INFO, "must split leaf within 'insertObjects' method");
                    return new SplitConfiguration(false, pppObjCount, objects.subList(pppObjCount, objects.size()), null);
                }
            }
            insertedObjs += next.getLocatorCount();
            pppObjCount ++;
        }
        objectCount += insertedObjs;
        return null;
    }
    
    
    // ***********************      Data readers       ********************************* //

    /**
     * Return iterator over all objects in the storage - the storage mustn't be null.
     * @return iterator over all objects in the storage
     */
    @Override
    public AbstractObjectIterator<PPPCodeObject> getAllObjects() {
        return (AbstractObjectIterator<PPPCodeObject>) getAllObjects(null, null, 0f);
    }

    public AbstractObjectIterator<? extends PPPCodeObject> getAllObjects(final PartialQueryPPPDistanceCalculator calculator, final float topDistance) {
        return getAllObjects(calculator, null, topDistance);
    }

    public AbstractObjectIterator<? extends PPPCodeObject> getAllObjects(final QueryPPPDistanceCalculator calculator, final short [] thisNodePPP) {
        return getAllObjects(calculator, thisNodePPP, 0f);
    }
    
    /**
     * Return iterator over all objects in the storage - the storage mustn't be null. If {@code onlyStoreSuffixes} is true then 
     *   the calculator is ignored and the query-object distance is not evaluated.
     * @param calculator calculator to calculate distances between a query object and the stored (and read) objects; if null 
     *  then it is not used
     * @param thisNodePPP this PPP is used as upper (higher) PP indexes of the PPPs of the returned data objects; 
     *   if null, the data objects are simply read from the memory and returned
     * @param topDistance
     * @return iterator over all objects in the storage
     */    
    protected AbstractObjectIterator<? extends PPPCodeObject> getAllObjects(final QueryPPPDistanceCalculator calculator, final short [] thisNodePPP, final float topDistance) {
        if (data == null) {
            return AbstractObjectIterator.emptyIterator();
        }
        
        final PPPCodeReadWriter serializer = getPPPCodeRW();
        final int thisLevel = (thisNodePPP == null) ? getLevel() : thisNodePPP.length;
        final int pppSufixLength = mIndex.getMaxLevel() - thisLevel;
        final ByteBufferBits readBuffer = serializer.createNewBuffer(data, getOccupationInBits());
        
        // create new iterator over all objects in this leaf cell
        return new AbstractObjectIterator<PPPCodeObject>() {

            PPPCodeObject currentObject = null;
            
            @Override
            public PPPCodeObject getCurrentObject() throws NoSuchElementException {
                return currentObject;
            }

            @Override
            public boolean hasNext() {
                if (currentObject != null) {
                    return true;
                }
                if (readBuffer.positionInBits() >= readBuffer.limitInBits()) {
                    return false;
                }
                try {
                    currentObject = serializer.readObject(readBuffer, pppSufixLength, calculator, thisNodePPP, thisLevel, topDistance);
                    return true;
                } catch (BufferUnderflowException ex) {
                    MetricIndexes.logger.log(Level.SEVERE, "CELL {0}: trying to read object from leaf cell illlegally", new Object [] { Arrays.toString(thisNodePPP)} );
                    ex.printStackTrace();
                    return false;
                }
            }

            @Override
            public PPPCodeObject next() {
                PPPCodeObject retVal = currentObject;
                currentObject = null;
                return retVal;
            }

            @Override
            public void remove() {
                throw new UnsupportedOperationException("This is a read only reader");
            }
        };
    }
    
    /**
     * This method returns list of all groups of objects (locators) with the same PPP-Code on level \lambda; the groups
     *  contain a "distance" from the query object.
     * @param calculator object that can calculate distance between a PPP-Code and current query object
     * @param thisNodePPP PPP-Code of this node
     * @param topDistance if the calculator is {@link PartialQueryPPPDistanceCalculator} than this is the distance to be added to its result
     * @return iterator over the list of all groups of objects stored by this node
     */
    public Iterator<RankableLocators> getRankableLocators(final QueryPPPDistanceCalculator calculator, final short [] thisNodePPP, final float topDistance) {
        if (data == null) {
            return Collections.EMPTY_LIST.iterator();
        }
        
        final PPPCodeReadWriter serializer = getPPPCodeRW();
        final int thisLevel = (thisNodePPP == null) ? getLevel() : thisNodePPP.length;
        final int pppSufixLength = mIndex.getMaxLevel() - thisLevel;
        final ByteBufferBits readBuffer = serializer.createNewBuffer(data, getOccupationInBits());
        
        // create new iterator over all objects in this leaf cell
        return new Iterator<RankableLocators>() {

            RankableLocators nextObject = null;
            
            @Override
            public boolean hasNext() {
                if (nextObject != null) {
                    return true;
                }
                if (readBuffer.positionInBits() >= readBuffer.limitInBits()) {
                    return false;
                }
                try {
                    nextObject = (calculator instanceof PartialQueryPPPDistanceCalculator) 
                            ? new LocatorsAndDistance(readBuffer, serializer, pppSufixLength, (PartialQueryPPPDistanceCalculator) calculator, thisLevel + 1, topDistance)
                            : new LocatorsAndDistance(readBuffer, serializer, pppSufixLength, calculator, thisNodePPP);
                    return true;
                } catch (BufferUnderflowException ex) {
                    MetricIndexes.logger.log(Level.SEVERE, "CELL {0}: trying to read object from leaf cell illlegally", new Object [] { Arrays.toString(thisNodePPP)} );
                    throw ex;
                }
            }

            @Override
            public RankableLocators next() {
                try {
                    return nextObject;
                } finally {
                    nextObject = null;
                }
            }

            @Override
            public void remove() {
                throw new UnsupportedOperationException("This is a read only reader");
            }
        };
    }
    
    @Override
    public int getObjectCountByObjects() {
        int i = 0;
        for (Iterator<PPPCodeObject> iterator = getAllObjects(); iterator.hasNext();) {
            i += iterator.next().getLocatorCount();
        }
        if (i != objectCount) {
            System.out.println("wrong object count in leaf " + Arrays.toString(getPppForReading()) +  " data say " + i + " but counter says " + objectCount + " - reparing");
            objectCount = i;
        }
        return i;
    }
    
    @Override
    public float reviseMinDistance(PartialQueryPPPDistanceCalculator calculator, float origDistance) {
        int thisLevel = getLevel();
        if (thisLevel >= mIndex.getMaxLevel()) {
            return origDistance;
        }
        PPPCodeReadWriter serializer = getPPPCodeRW();
        return origDistance  +  serializer.getMinPPPDistance(serializer.createNewBuffer(data, getOccupationInBits()), 
                mIndex.getMaxLevel() - thisLevel, calculator, thisLevel);
    }    
    
    // **************     Handling of delete operation      *********************** //
    
    /**
     * Deletes given object from this leaf according to locator (ID).
     * @param objects to be deleted
     * @param deleteLimit maximum number of objects that match given object to be deleted
     * @param checkLocator if true, then the object is deleted according to object locator
     * @return number of objects actually deleted by this method
     * @throws AlgorithmMethodException 
     */
    @Override
    public synchronized Collection<PPPCodeObject> deleteObjects(List<PPPCodeObject> objects, int deleteLimit, boolean checkLocator) throws AlgorithmMethodException {
        // create a map between the PPPs and sets of locators to be deleted for each of the PPP
        Map<ShortArray,Set<Integer>> locatorsToDelete = initPPPLocatorMap(objects);

        int actuallyDeleted = 0;
        boolean allDeleted = false;
        while (! allDeleted) {
            try {
                actuallyDeleted += findAndMarkForDelete(locatorsToDelete);
                allDeleted = true;
            } catch (IllegalArgumentException e) {
                consolidateData(null);
            }            
        }
        objectCount -= actuallyDeleted;
        
        return findNotDeletedObjects(locatorsToDelete, objects);
    }

    /**
     * Initializes and returns a map of {@link ShortArray}-encoded PPPs with respective
     *  integer-encoded IDs for given objects.
     */
    private Map<ShortArray, Set<Integer>> initPPPLocatorMap(List<PPPCodeObject> objects) {
        Map<ShortArray,Set<Integer>> locatorsToDelete = new HashMap<>(objects.size());
        // the sent object can theoretically contain more than one locator
        for (PPPCodeObject object : objects) {
            ShortArray pppArray = new ShortArray(Arrays.copyOfRange(object.getPppForReading(),
                    getLevel(), object.getPPPLength()));
            Set<Integer> locators = locatorsToDelete.get(pppArray);
            if (locators == null) {
                locators = new HashSet<>(objects.size());
                locatorsToDelete.put(pppArray, locators);
            }
            for (int loc : object.getLocators()) {
                locators.add(loc);
            }
        }
        return locatorsToDelete;
    }

    private int findAndMarkForDelete(Map<ShortArray, Set<Integer>> locatorsToDelete) throws IllegalArgumentException {
        // iterate over all stored objects and try to find the locator(s) to remove
        int actuallyDeleted = 0;
        int locIndex = 0;
        MainLoop:
        for (Iterator<PPPCodeObject> allObjs = getAllObjects(); allObjs.hasNext();) {
            PPPCodeObject next = allObjs.next();
            //for (Map.Entry<ShortArray, Set<Integer>> entry : locatorsToDelete.entrySet()) {
            Set<Integer> locators = locatorsToDelete.get(new ShortArray(next.getPppForReading()));
            if (locators != null) {
                for (int loc : next.getLocators()) {
                    if (locators.contains(loc)) {
                        addDeletedIndex(locIndex);
                        locators.remove(loc);
                        actuallyDeleted ++;
                        
                        // if we have deleted all locators to be deleted, end the method
                        if (locators.isEmpty()) {
                            locatorsToDelete.remove(new ShortArray(next.getPppForReading()));
                        }
                        if (locatorsToDelete.isEmpty()) {
                            break MainLoop;
                        }
                    }
                    locIndex ++;                    
                }
            } else {
                locIndex += next.getLocatorCount();
            }
        }
        return actuallyDeleted;
    }

    /**
     * Mark given locator as "deleted" by inserting it into the sorted array {@link #deletedIndexes}.
     * @param index index (order) of the object that was deleted
     * @throws IllegalArgumentException if the passed index is already within the deleted indexes
     */
    protected void addDeletedIndex(int index) throws IllegalArgumentException {
        if (deletedIndexes == null) {
            deletedIndexes = new int [] {index};
        } else {
            // check, if the index is NOT in the deleted indexes already; if yes, throw an exception
            for (int deletedIndex : deletedIndexes) {
                if (index == deletedIndex) {
                    throw new IllegalArgumentException("cannot delete the same object twice");
                }
            }
            deletedIndexes = Arrays.copyOf(deletedIndexes, deletedIndexes.length + 1);
            int i = 0;
            while (i < deletedIndexes.length - 1 && deletedIndexes[i] < index) {
                i++;
            }
            if (i < deletedIndexes.length - 1) {
                System.arraycopy(deletedIndexes, i, deletedIndexes, i+1, deletedIndexes.length - i - 1);
            }
            deletedIndexes[i] = index;
        }
    }
    
    /** 
     * Given a list map of not-deleted PPPs+IDs, this method finds corresponding PPPCOdeObjects from
     *  within the given list.
     */
    private Collection<PPPCodeObject> findNotDeletedObjects(Map<ShortArray, Set<Integer>> locatorsToDelete, List<PPPCodeObject> objects) {
        if (locatorsToDelete.isEmpty()) {
            return Collections.emptyList();
        }
        // return the not deleted objects
        Collection<PPPCodeObject> notDeleted = new ArrayList<>();
        for (Set<Integer> notDeletedIDs : locatorsToDelete.values()) {
            for (PPPCodeObject obj : objects) {
                for (int id : obj.getLocators()) {
                    if (notDeletedIDs.contains(id)) {
                        notDeleted.add(obj);
                        break;
                    }
                }
            }
        }
        return notDeleted;
    }
    
    /**
     * Filters out the deleted locators out from given object with locators. 
     * @param firstLocatorIndex index of the first locator in the passed object within all leaf objects
     * @param object object to remove locators from
     * @return object with some locators filtered out or NULL if all the locators were removed
     */
    private PPPCodeObject removeDeletedIndexes(int firstLocatorIndex, PPPCodeObject object) {
        // remove the deleted objects
        if (deletedIndexes == null || deletedIndexes[0] >= firstLocatorIndex + object.getLocatorCount()) {
            return object;
        }
        int [] clearedLocs = object.getLocators();
        int deleted = 0;
        while (deleted < deletedIndexes.length &&  deletedIndexes[deleted] < firstLocatorIndex + object.getLocatorCount()) {
            int indexToRemove = deletedIndexes[deleted] - firstLocatorIndex - deleted;
            System.arraycopy(clearedLocs, indexToRemove + 1, clearedLocs, indexToRemove, clearedLocs.length - indexToRemove - 1);
            deleted ++;
        }
        if (deleted >= deletedIndexes.length) {
            deletedIndexes = null;
        } else {
            deletedIndexes = Arrays.copyOfRange(deletedIndexes, deleted, deletedIndexes.length);
        }
        if (clearedLocs.length == deleted) {
            return null;
        }
        return new PPPCodeObject(Arrays.copyOf(clearedLocs, clearedLocs.length - deleted), object.getPppForReading());
    }
        
    
    /**
     * This method should be called only when the {@code insertObjects} method returns non-empty list of objects
     * (= {@code CapacityFullException} was throw during insertion of objects into bucket managed by this LeafCell)
     * or when the {@code checkAgileMultibucketPartitioning} returns true
     * @return
     * @throws AlgorithmMethodException 
     */
    @Override
    public boolean split(SplitConfiguration splitConfiguration) throws AlgorithmMethodException {
        if (! isValid()) {
            return false;
        }
        consolidateData(null);
        invalidateThisNode(split(getAllObjects(), 0));
        return true;
    }

    /**
     * It actually splits the
     * @param it
     * @param pppIndexToConsider
     * @return
     * @throws AlgorithmMethodException 
     */
    private VoronoiInternalCell<PPPCodeObject> split(Iterator<PPPCodeObject> it, int pppIndexToConsider) throws AlgorithmMethodException {
        short [] thisNodePPP = parentNode.getChildPPP(this);
        VoronoiInternalCell newNode = (VoronoiInternalCell) parentNode.createSpecificChildCell(true, thisNodePPP);

        // sort the data according to the currently first pivot
        List<List<PPPCodeObject>> newLeafData = new ArrayList<>(mIndex.getNumberOfPivots());
        for (int i = 0; i < mIndex.getNumberOfPivots(); i++) {
            newLeafData.add(null);         
        }        
        while (it.hasNext()) {
            PPPCodeObject next = it.next();
            List<PPPCodeObject> subList = newLeafData.get(next.getIndexAt(pppIndexToConsider));
            if (subList == null) {
                newLeafData.set(next.getIndexAt(pppIndexToConsider), subList = new ArrayList<>());
            }
            subList.add(next);
        }
        
        // create the new leaf cells and insert data into them
        for (short i = 0; i < mIndex.getNumberOfPivots(); i++) {
            if (newLeafData.get(i) == null) {
                continue;
            }
            PPPCodeLeafCell newLeaf = (PPPCodeLeafCell) newNode.createChildNode(i, false);
            if (newLeaf == null) {
                MetricIndexes.logger.log(Level.WARNING, "repeated index in PPP, within cell {0}; index: {1}", new Object[]{thisNodePPP, i});
            } else {
                if (newLeaf.insertObjects(newLeafData.get(i)) != null) {
                    VoronoiInternalCell<PPPCodeObject> substitute = newLeaf.split(newLeafData.get(i).iterator(), pppIndexToConsider + 1);
                    newLeaf.invalidateThisNode(substitute);
                }
            }
        }
        
        parentNode.putChildNode(thisNodePPP[thisNodePPP.length - 1], newNode);
        return newNode;
    }    

    @Override
    public int processOperation(QueryOperation operation) throws AlgorithmMethodException {
        throw new UnsupportedOperationException();
    }
    

    // **********************************       Consolidation of the content      ************************** //
    
    /**
     * Merges together all data objects with the same PPP-Codes and filters out the deleted IDs.
     * @param temporary a buffer to be used for the consolidation (so that it does not have to be created for each leaf)
     * @return true, if the consolidation actually took place
     */
    protected synchronized boolean consolidateData(byte [] temporary) {
        // if the node is either empty, or it was not modified, it does not need any consolidation
        if (data == null || (writeBufferBits == null && deletedIndexes == null)) {
            return false;
        }
        if (temporary == null) {
            temporary = new byte[1024 * 1024]; 
        }
        
        // read all the objects and put together those with the same PPP
        Map<ShortArray, List<PPPCodeObject>> readObjects = new HashMap<>();
        int objIndex = 0;
        for (AbstractObjectIterator<PPPCodeObject> it = getAllObjects(); it.hasNext();) {
            PPPCodeObject next = it.next();
            int locCount = next.getLocatorCount();
            next = removeDeletedIndexes(objIndex, next);
            objIndex += locCount;
            // if all locators were deleted
            if (next == null) {
                continue;
            }
            ShortArray nextPPPCode = new ShortArray(next.getPppForReading());
            List<PPPCodeObject> existing = readObjects.get(nextPPPCode);
            if (existing == null) {
                readObjects.put(nextPPPCode, existing = new ArrayList());
            }                
            existing.add(next);
        }

        // create a single PPPCodeObject for all objects with the same PPP-Code
        List<PPPCodeObject> newObjects = new ArrayList<>(readObjects.size());
        for (List<PPPCodeObject> objsToMerge : readObjects.values()) {
            if (objsToMerge.size() == 1) {
                newObjects.add(objsToMerge.get(0));
                continue;
            }
            int idsLength = 0;
            int [] [] idsToMerge = new int[objsToMerge.size()][];
            int counter = 0;
            for (PPPCodeObject pPPCodeObject : objsToMerge) {
                idsToMerge[counter ++] = pPPCodeObject.getLocators();
                idsLength += pPPCodeObject.getLocatorCount();
            }
            newObjects.add(new PPPCodeObject(idsToMerge, idsLength, objsToMerge.get(0).getPppForReading()));
        }            

        // write the consolidated objects to the temp. byte buffer
        PPPCodeReadWriter pppCodeReadWriter = getPPPCodeRW();
        ByteBufferBits temporaryBuffer;
        boolean overflow;
        do {
            overflow = false;
            temporaryBuffer = pppCodeReadWriter.createNewBuffer(temporary);
            try {
                for (PPPCodeObject obj : newObjects) {
                    obj.writeToBytes(temporaryBuffer, pppCodeReadWriter);
                }
            } catch (BufferOverflowException ex) {
                overflow = true;
                temporary = new byte[(int) (temporary.length * ARRAY_INCREMENT_COEF)];
            }
        } while (overflow);

        // shrink the buffer size to the smallest possible length
        data = Arrays.copyOf(temporary, temporaryBuffer.position());
        positionBits = temporaryBuffer.positionInBits();
        writeBufferBits = null;
        return true;
    }

    /**
     * Auxiliary class that wraps array and equals/hashCode is defined by comparing array items. Used during
     *  the consolidation of the leaf.
     */
    private static class ShortArray {
        
        private final short [] array;

        public ShortArray(short[] array) {
            this.array = array;
        }
        
        @Override
        public boolean equals(Object obj) {
            if (! (obj instanceof ShortArray)) {
                return false;
            }
            return Arrays.equals(array, ((ShortArray) obj).array);
        }

        @Override
        public int hashCode() {
            return Arrays.hashCode(array);
        }
    }
    
    @Override
    public void clearData() throws AlgorithmMethodException {
        throw new UnsupportedOperationException();
//        if (storage != null) {
//            mIndex.removeBucket(storage, true);
//            storage = null;
//        }
    }

    @Override
    public void calculateTreeSize(Map<Short,AtomicInteger> intCellNumbers, Map<Short, AtomicLong> branchingSums, 
            AtomicInteger leafCellNumber, AtomicLong dataSizeBytes, AtomicLong leafLevelSum) {
        leafCellNumber.incrementAndGet();
        leafLevelSum.addAndGet(getLevel());
        dataSizeBytes.addAndGet(getOccupationInBytes());
    }
    
    // ******************    Testing methods    ********************* //
    
    public boolean findObject(int id) {
        AbstractObjectIterator<PPPCodeObject> allObjects = getAllObjects();
        while (allObjects.hasNext()) {
            if (contains(allObjects.next(), id)) {
                new Exception().printStackTrace();
                return true;
            }
        }
        return false;
    }    

    protected final boolean contains(PPPCodeObject obj, int id) {
        for (int dataId : obj.getLocators()) {
            if (dataId == id) {
                System.out.println("Found id " + dataId + " in " + Arrays.toString(obj.getPppForReading()) + ": " + Arrays.toString(obj.getLocators()) + " in cell " + Arrays.toString(getPppForReading()));
                return true;
            }
        }
        return false;
    }
}
