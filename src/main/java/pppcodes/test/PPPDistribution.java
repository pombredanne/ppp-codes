/*
 *  This file is part of PPP-Codes library: http://disa.fi.muni.cz/results/software/ppp-codes/
 *
 *  PPP-Codes library is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  PPP-Codes library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with PPP-Codes library.  If not, see <http://www.gnu.org/licenses/>.
 */
package pppcodes.test;

/*
 *  This file is part of MESSIF library.
 *
 *  MESSIF library is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  MESSIF library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with MESSIF library.  If not, see <http://www.gnu.org/licenses/>.
 */
import gnu.trove.list.TIntList;
import gnu.trove.list.array.TIntArrayList;
import java.io.IOException;
import java.io.PrintStream;
import java.util.Arrays;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.NoSuchElementException;
import messif.algorithms.AlgorithmMethodException;
import messif.objects.LocalAbstractObject;
import messif.objects.util.StreamGenericAbstractObjectIterator;
import pppcodes.PPPCodeIndex;
import pppcodes.algorithms.PPPCodeSingleAlgorithm;
import pppcodes.index.PPPCodeObject;

/**
 * Allows to compute a distance histogram of a given metric space.
 * The metric space is represented by the data object class, the distances
 * are measured on a sample taken from a given data file.
 *
 * <p>
 * Note that the distances are computed in a stream fashion, so the data file
 * should be randomized first.
 * </p>
 *
 * @author David Novak, Masaryk University, Brno, Czech Republic, novak.david@gmail.com
 */
public class PPPDistribution {

    public static void analyzePPPDistribution(PPPCodeSingleAlgorithm alg, String dataFile, int maxObjects, int maxLevel, boolean printValues) {
        System.out.println("Running PPPDistribution on data file: " + dataFile + ", maxLevel: " + maxLevel);
        PPPCodeIndex pppIndex = alg.getmIndex();
        try {
            Map<ShortArray, TIntList> readObjects = new HashMap<>();
            int objsRead = 0;
            try (StreamGenericAbstractObjectIterator<LocalAbstractObject> iterator = new StreamGenericAbstractObjectIterator<>(pppIndex.getObjectClass(), dataFile)) {
                while (iterator.hasNext() && objsRead < maxObjects) {
                    PPPCodeObject pppObject = (PPPCodeObject) pppIndex.initPP(iterator.next());
                    ShortArray nextPPPCode = new ShortArray(pppObject.getPppForReading(), maxLevel);
                    TIntList existing = readObjects.get(nextPPPCode);
                    if (existing == null) {
                        readObjects.put(nextPPPCode, existing = new TIntArrayList());
                    }
                    existing.add(pppObject.getLocators());
                    ++ objsRead;
                }
            }
            
            // sort the grouped PPPs by the size
            Map.Entry<ShortArray, TIntList> pppIDs [] = readObjects.entrySet().toArray(new Map.Entry [0]);
            Arrays.sort(pppIDs, new Comparator<Map.Entry<ShortArray, TIntList>>() {
                @Override
                public int compare(Map.Entry<ShortArray, TIntList> o1, Map.Entry<ShortArray, TIntList> o2) {
                    return Integer.compare(o2.getValue().size(), o1.getValue().size());
                }
            });

            if (printValues) {
                for (Map.Entry<ShortArray, TIntList> pppID : pppIDs) {
                    System.out.println(Arrays.toString(pppID.getKey().array) + ": " + pppID.getValue().size() + " (" + pppID.getValue().toString()+")");
                }
            }

            for (Map.Entry<ShortArray, TIntList> pppID : pppIDs) {
                System.out.println(((float) pppID.getValue().size() / (float) objsRead) * 100 + "%");
            }            
            
        } catch (IOException | IllegalArgumentException | IllegalStateException | NoSuchElementException | AlgorithmMethodException e) {
            e.printStackTrace();
        }
    }
    
    private static class ShortArray {

        private final short[] array;

        public ShortArray(short[] array, int maxLevel) {
            this.array = Arrays.copyOf(array, maxLevel);
        }

        @Override
        public boolean equals(Object obj) {
            if (!(obj instanceof ShortArray)) {
                return false;
            }
            return Arrays.equals(array, ((ShortArray) obj).array);
        }

        @Override
        public int hashCode() {
            return Arrays.hashCode(array);
        }
    }

}
