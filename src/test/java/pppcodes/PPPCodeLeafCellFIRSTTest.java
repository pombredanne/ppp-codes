/*
 *  This file is part of PPP-Codes library: http://disa.fi.muni.cz/results/software/ppp-codes/
 *
 *  PPP-Codes library is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  PPP-Codes library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with PPP-Codes library.  If not, see <http://www.gnu.org/licenses/>.
 */
package pppcodes;

import pppcodes.index.PPPCodeReadWriter;
import pppcodes.index.PPPCodeObject;
import pppcodes.index.PPPCodeInternalCell;
import pppcodes.index.PPPCodeLeafCell;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import java.util.Map.Entry;
import java.util.Random;
import messif.algorithms.AlgorithmMethodException;
import messif.objects.util.AbstractObjectIterator;
import messif.operations.QueryOperation;
import mindex.MetricIndex;
import mindex.distance.QueryPPPDistanceCalculator;
import mindex.navigation.SplitConfiguration;
import mindex.navigation.VoronoiCell;
import org.jmock.Expectations;
import org.jmock.Mockery;
import org.jmock.integration.junit4.JMock;
import org.jmock.integration.junit4.JUnit4Mockery;
import org.jmock.lib.legacy.ClassImposteriser;
import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

/**
 *
 * @author David Novak, Masaryk University, Brno, Czech Republic, novak.david@gmail.com
 */
@RunWith(JMock.class)
public class PPPCodeLeafCellFIRSTTest {
    
    private Mockery context = new JUnit4Mockery() {
            {
                setImposteriser(ClassImposteriser.INSTANCE);
            }
        };
    private PPPCodeIndex mIndexMock;
    private Random random = new Random(System.currentTimeMillis());
    private QueryPPPDistanceCalculator calculatorMock;
    //private int MAX_PIVOTS = 100;
    
    public PPPCodeLeafCellFIRSTTest() {

    }

    short [] ppp;
    
    @Before
    public void setUp() throws AlgorithmMethodException {
        System.out.println("running setup");
        mIndexMock = context.mock(PPPCodeIndex.class);
        calculatorMock = context.mock(QueryPPPDistanceCalculator.class);
        final short maxLevel = (short) 2;
        context.checking(new Expectations() {
            {
                allowing(mIndexMock).getMaxLevel();
                will(returnValue(maxLevel));
                allowing(mIndexMock).getNumberOfPivots();
                will(returnValue((short) 100));
                allowing(mIndexMock).getMaxObjectNumber();
                will(returnValue(0L));
                allowing(mIndexMock).getPPPCodeReadWriter();
                will(returnValue(new PPPCodeReadWriter(100, (short) maxLevel, 0L)));
                allowing(mIndexMock).setDynamicTreeModified(with(any(boolean.class)));
                allowing(mIndexMock).getBucketCapacity();
                will(returnValue(0));
                allowing(mIndexMock).isBucketOccupationInBytes();
                will(returnValue(Boolean.TRUE));
                allowing(calculatorMock).getQueryDistance(with(any(short [].class)));
                will(returnValue(0f));
            }
        });
        
        PPPCodeInternalCell pppCodeInternalCell = new PPPCodeInternalCell(mIndexMock, null, new short[0]);
        leaf = (PPPCodeLeafCell) pppCodeInternalCell.createChildNode((short) 5, false);
        //leaf = new PPPCodeLeafCell((short) 1, mIndexMock, null, new int[] {5, 4, 3, 2, 1});
    }

    public PPPCodeLeafCell leaf;    
    protected short [] leafPPP = new short [] {(short)5 };
    protected List<PPPCodeObject> insertedObjects = new ArrayList<>();
    
    /**
     * Test of insertObjects method, of class PPPCodeLeafCell.
     */
    @Test
    public void testInsertObjects() throws Exception {
        System.out.println("insertObjects");
        insertedObjects = new ArrayList<>();
        insertObjects(10);
    }

    
    protected int BATCH_SIZE = 100;
    
    public SplitConfiguration insertObjects(int numberToInsert) throws Exception {
        List<PPPCodeObject> objectsToInsert = new ArrayList<>(BATCH_SIZE);
        for (int j = 0; j < numberToInsert; j++) {
            objectsToInsert.add(crateRandomPPPCodeObject(leafPPP));
            if (objectsToInsert.size() == BATCH_SIZE || j == numberToInsert - 1) {
                SplitConfiguration splitConfig;
                if ((splitConfig = leaf.insertObjects(objectsToInsert)) == null) {
                    insertedObjects.addAll(objectsToInsert);
                } else {
                    return splitConfig;
                }
            }
        }
        return null;
    }

    protected PPPCodeObject crateRandomPPPCodeObject(short [] prefixToUse) {
        short[] array = new short[mIndexMock.getMaxLevel()];
        System.arraycopy(prefixToUse, 0, array, 0, prefixToUse.length);
        for (int i = prefixToUse.length; i < array.length; i++) {
            do {
                array[i] = (short) random.nextInt(mIndexMock.getNumberOfPivots());
                for (int j = 0; j < i; j++) {
                    if (array[i] == array[j]) {
                        array[i] = (short) -1;
                    }
                }
            } while (array[i] < 0);
        }
        PPPCodeObject pppCodeObject = new PPPCodeObject(Math.abs(random.nextInt()), array);
        return pppCodeObject;
    }
    
    /**
     * Test of getAllObjects method, of class PPPCodeLeafCell.
     */
    @Test
    public void testGetAllObjects() throws Exception {
        System.out.println("getAllObjects");
        insertedObjects = new ArrayList<>();
        insertObjects(100);
        AbstractObjectIterator result = leaf.getAllObjects(calculatorMock, leafPPP);
        for (PPPCodeObject inserted : insertedObjects) {
            if (! result.hasNext()) {
                fail("reading fewer objects than written to leaf node");
            }
            PPPCodeObject readObject = (PPPCodeObject) result.next();
            assertEquals(inserted.getLocators()[0], readObject.getLocators()[0]);
            //assertArrayEquals(Arrays.copyOfRange(inserted.getPpp(),1,inserted.getPpp().length), readObject.getPpp());            
            assertArrayEquals(inserted.getPppForReading(), readObject.getPppForReading());
        }
        assertFalse(result.hasNext());
    }

    /**
     * Test of split method, of class PPPCodeLeafCell.
     */
    @Test
    public void testSplit() throws Exception {
        System.out.println("split");
        SplitConfiguration config = null;
        while ((config = insertObjects(1000)) == null);
        assertTrue(leaf.split(config));
        
//        AbstractObjectIterator allObjects = leaf.getParentNode().getAllObjects();
        int number = getNumberOfObjects(leaf.getParentNode(), Arrays.copyOf(leafPPP, leafPPP.length - 1));
        
//        while (allObjects.hasNext()) {
//            allObjects.next();
//            number ++;
//        }
        //assertEquals(, insertedObjects.size());
        assertEquals(insertedObjects.size(), number);
    }

    @Test
    public void justRun() throws Exception {
        
        assertEquals(8, Byte.SIZE);
    }
    
    private int getNumberOfObjects(VoronoiCell cell, short [] nodePPP) {
        if (cell instanceof PPPCodeInternalCell) {
            int ret = 0;
            for (Iterator it = ((PPPCodeInternalCell) cell).getChildNodes(); it.hasNext();) {
                Entry<Short, VoronoiCell> child = (Entry<Short, VoronoiCell>) it.next();
                short[] childPPP = Arrays.copyOf(nodePPP, nodePPP.length + 1);
                childPPP[childPPP.length - 1] = child.getKey();
                ret += getNumberOfObjects(child.getValue(), childPPP);
            }
            return ret;
        }
        if (cell instanceof PPPCodeLeafCell) {
            int ret = 0;
            AbstractObjectIterator<? extends PPPCodeObject> allObjects = ((PPPCodeLeafCell) cell).getAllObjects(calculatorMock, nodePPP);
            while (allObjects.hasNext()) {
                ret += allObjects.next().getLocatorCount();
            }
            return ret;
        }
        throw new IllegalArgumentException("something else than a PPPCOde cell");
    }

    /**
     * Test of processOperation method, of class PPPCodeLeafCell.
     */
    //@Test
    public void testProcessOperation() throws Exception {
        System.out.println("processOperation");
        QueryOperation operation = null;
        PPPCodeLeafCell instance = null;
        int expResult = 0;
        int result = instance.processOperation(operation);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of clearData method, of class PPPCodeLeafCell.
     */
    //@Test
    public void testClearData() throws Exception {
        System.out.println("clearData");
        PPPCodeLeafCell instance = null;
        instance.clearData();
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

}
